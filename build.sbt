import AssemblyKeys._
/* basic project info */
name := "weka"

organization := "com.fenixinvesting"

version := "1.0.0-SNAPSHOT"

description := "Scala wrapper around some basic features of Weka"

//homepage := Some(url("https://github.com/pkaeding/myproj"))

startYear := Some(2013)

/* scala versions and options */
scalaVersion := "2.10.1"

crossScalaVersions := Seq("2.9.2", "2.10.1")

offline := false

scalacOptions ++= Seq("-deprecation", "-unchecked")

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

/* entry point */
mainClass in (Compile, packageBin) := Some("com.fenixinvesting.weka.Main")

mainClass in (Compile, run) := Some("com.fenixinvesting.weka.Main")

net.virtualvoid.sbt.graph.Plugin.graphSettings

/* dependencies */
libraryDependencies ++= Seq (
  "commons-io" % "commons-io" % "2.4",
  "de.jflex" % "jflex" % "1.4.3",
  "nz.ac.waikato.cms.weka" % "weka-stable" % "3.6.9" excludeAll(
    ExclusionRule(organization = "net.sf.squirrel-sql.thirdparty-non-maven")
  ),
  // scalaz dep defined in Build.scala
  // grizzled-slf4j dep defined in Build.scala
  "org.scalacheck" %% "scalacheck" % "1.10.0" % "test"
)

/* you may need these repos */
resolvers ++= Seq(
  // Resolvers.sonatypeRepo("snapshots")
  // Resolvers.typesafeIvyRepo("snapshots")
  // Resolvers.typesafeIvyRepo("releases")
  // Resolvers.typesafeRepo("releases")
  // Resolvers.typesafeRepo("snapshots")
  // JavaNet2Repository,
  // JavaNet1Repository
)

/* sbt behavior */
logLevel in compile := Level.Warn

traceLevel := 5

publishTo <<= version { (v: String) =>
  val base = "/Users/pkaeding/Dropbox/public/maven-repo/"
  if (v.trim.endsWith("SNAPSHOT"))
    Some(Resolver.file("file",  new File( base + "snapshots" )) )
  else
    Some(Resolver.file("file",  new File( base + "releases" )) )
}

releaseSettings

ReleaseKeys.releaseProcess <<= thisProjectRef apply { ref =>
  import sbtrelease._
  import ReleaseStateTransformations._
  Seq[ReleasePart](
    checkSnapshotDependencies,              // : ReleasePart
    inquireVersions,                        // : ReleasePart
    setReleaseVersion,                      // : ReleasePart
    commitReleaseVersion,                   // : ReleasePart
    tagRelease,                             // : ReleasePart
    publishArtifacts,
    setNextVersion,                         // : ReleasePart
    commitNextVersion                       // : ReleasePart
  )
}

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra := (
  <developers>
    <developer>
      <id>pkaeding</id>
      <name>Patrick Kaeding</name>
      <email>patrick@kaeding.name</email>
      <!-- <url></url> -->
    </developer>
  </developers>
)

// Josh Suereth's step-by-step guide to publishing on sonatype
// httpcom://www.scala-sbt.org/using_sonatype.html

/* assembly plugin */
assemblySettings

mergeStrategy in assembly := {
  case s if s.endsWith("MANIFEST.MF") => MergeStrategy.rename
  case _ => MergeStrategy.last
}

excludedJars in assembly <<= (fullClasspath in assembly) map { cp => 
  cp filter {_.data.getName != "java-cup.jar"}
}

test in AssemblyKeys.assembly := {}

artifact in (Compile, assembly) ~= { art =>
  art.copy(`classifier` = Some("assembly"))
}

addArtifact(artifact in (Compile, assembly), assembly)

crossPaths in assembly := true

target in assembly <<= crossTarget

//jarName in assembly <<= artifactName

defaultJarName in assembly <<= (scalaBinaryVersion, moduleName) { (sv, id) => id + "-assembly_" + sv + ".jar" }