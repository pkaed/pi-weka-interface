package com.fenixinvesting.weka

import scalaz._, Scalaz._

import java.util.{ Calendar, Date, Scanner }
import java.lang.Integer
import java.text.SimpleDateFormat
import grizzled.slf4j.Logger
import weka.core.{ FastVector, Attribute, Instances, Instance, SerializationHelper }
import weka.filters.Filter
import weka.classifiers.bayes.BayesNet
import weka.classifiers._

case class ArffField[A](name: String, value: A => Option[Double])

trait ARFFFileGenerator[A] {
  val logger = Logger[this.type]
  val fields: Seq[ArffField[A]]
  def classifier: Classifier
  lazy val attributes = {
    val ret = new FastVector(fields.length)
    fields.map(f ⇒ ret.addElement(new Attribute(f.name)))
    ret
  }

  def createInstance(a: A): Instance = {
    val values = fields.map(_.value(a).getOrElse(Instance.missingValue)).toArray
    val ret = new Instance(1.0, values)
    val string = ret.toString
    if (string == null || string == "null") {
      logger.error("Found 'null' value from optionChain: %s" format a.toString)
    }
    ret
  }

  def trainClassifier(train: Instances, filenamePrefix: String) = {
    logger.info("Training classifier")
    val c = classifier
    c.buildClassifier(train)
    logger.info("Outputting Classifier to %s/classifier.model" format filenamePrefix)
    val header = new Instances(train, 0)
    SerializationHelper.writeAll(filenamePrefix + "/classifier.model", Array[Object](c, header))
    c
  }

  def testClassifier(c: Classifier, train: Instances)(test: Instances) = {
    logger.info("Testing Classifier")
    val eval = new Evaluation(train)
    eval.evaluateModel(c, test)
    logger.info("Classifier test results: \n%s" format eval.toSummaryString(true))
  }

  def createAndDiscretizeInstances(startInstances: Instances, fileNamePrefix: String, filter1: Filter, filter2: Filter, isTrainingData: Boolean) = {
    outputArffFile(startInstances, fileNamePrefix + ".arff")

    def runFilter(filter: Filter, instances: Instances) = {
      if (isTrainingData) filter.setInputFormat(instances)
      Filter.useFilter(instances, filter)
    }

    val filteredOnce = runFilter(filter1, startInstances)
    outputArffFile(filteredOnce, fileNamePrefix + "_filtered1.arff")

    filteredOnce.setClassIndex(attributes.size - 1)
    val filteredTwice = runFilter(filter2, filteredOnce)
    outputArffFile(filteredTwice, fileNamePrefix + "_filtered2.arff")
    filteredTwice
  }

  def createFilter1: Filter = {
    import weka.filters.unsupervised.attribute.Discretize
    val filter = new Discretize
    // first discretize the class attribute with the unsupervized filter
    filter.setAttributeIndicesArray(Array(attributes.size - 1))
    filter.setBins(200)
    logger.info("Unsupervised Discretizer options: %s" format filter.getOptions.toList)
    filter
  }
  def createFilter2: Filter = {
    import weka.filters.supervised.attribute.Discretize
    val filter = new Discretize
    // next use the supervised filter on the other attributes
    filter.setAttributeIndicesArray(Array(attributes.size - 1))
    filter.setInvertSelection(true)
    logger.info("Supervised Discretizer options: %s" format filter.getOptions.toList)
    filter
  }

  def outputArffFile(instances: Instances, fileName: String) {
    import org.apache.commons.io.FileUtils
    import java.io.File
    val file = new File(fileName)
    FileUtils.writeStringToFile(file, instances.toString)
    try {
      logger.info("Wrote instances to file: %s\n%s".format(file.getAbsolutePath, instances.toSummaryString))
    } catch {
      case e: Throwable ⇒ logger.error("Encountered error when trying to summarize the arff file", e)
    }
  }
}