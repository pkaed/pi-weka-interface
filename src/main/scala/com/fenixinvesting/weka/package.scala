package com.fenixinvesting

import java.util.{Calendar, Date}

package object weka {
  implicit def int2Double(i: Integer): Double = i.doubleValue
  implicit def intOpt2DoubleOpt(i: Option[Integer]): Option[Double] = i.map(_.doubleValue)
  implicit def longOpt2DoubleOpt(i: Option[Long]): Option[Double] = i.map(_.doubleValue)
  implicit def date2Calendar(d: Date): Calendar = {
    val cal = Calendar.getInstance()
    cal.setTime(d)
    cal
  }
}