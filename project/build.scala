import sbt._
import Keys._

object build extends Build {

  val gcsettings = Defaults.defaultSettings

  val gc = TaskKey[Unit]("gc", "runs garbage collector")
  val gcTask = gc := {
    println("requesting garbage collection")
    System gc()
  }

  lazy val project = Project(
    "project",
    file("."),
    settings = gcsettings ++ Seq(gcTask
        , libraryDependencies <+= scalaVersion(scalazDependency(_))
        , libraryDependencies <+= scalaVersion(slf4jDependency(_))
        ))

  def scalazDependency(scalaVersion: String) = scalaVersion match {
    case "2.9.2" ⇒ "org.scalaz" %% "scalaz-core" % "7.0.0-M4"
    case _       ⇒ "org.scalaz" %% "scalaz-core" % "7.0.0-M9"
  }
  
  def slf4jDependency(scalaVersion: String) = scalaVersion match {
    case "2.9.2" ⇒ "org.clapper" %% "grizzled-slf4j" % "0.6.10"
    case _       ⇒ "org.clapper" %% "grizzled-slf4j" % "1.0.1"
  }
}
