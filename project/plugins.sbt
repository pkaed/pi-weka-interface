addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.8.4")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "0.7")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.7.1")