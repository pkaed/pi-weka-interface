# PI Weka Interface — Scala wrapper around some basic features of Weka #

## Build & run ##

```sh
$ cd PI-Weka-Interface
$ ./sbt
> run
```

## Contact ##

- Patrick Kaeding
- <a href="patrick@kaeding.name">patrick@kaeding.name</a>
